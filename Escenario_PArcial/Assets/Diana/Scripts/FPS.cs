﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPS : MonoBehaviour {

	[SerializeField] private Text fps;
	[SerializeField] private float [] fpsHistory;
	private int counter;	

	// Update is called once per frame
	void Update () {
		counter++; 
		if (counter == fpsHistory.Length)
		{
			counter = 0;
			float total = 0;
			for (int i = 0; i < fpsHistory.Length; i++) 
			{
				total += fpsHistory [i];

			}
			fps.text = (total / fpsHistory.Length).ToString ("N1");
		}
		fpsHistory [counter] = 1 / Time.deltaTime;
	}
}
