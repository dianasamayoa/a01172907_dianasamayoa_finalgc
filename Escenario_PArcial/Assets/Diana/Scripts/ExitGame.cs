﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitGame : MonoBehaviour {

    [SerializeField] GameObject canvasOb; 
    void Start () {
        StartCoroutine(ExitScene());
	}

    IEnumerator ExitScene()
    {
        yield return new  WaitForSeconds(25f);

        canvasOb.SetActive(true);

    }
	
}
