﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ChangeScene : MonoBehaviour {
    public string sceneRocks;
    public string scenePolice;
    public Color loadToColor = Color.white;
    public void LoadRocks()
    {
        Initiate.Fade(sceneRocks, loadToColor, 0.7f);
    }
    public void LoadPolice()
    {
        Initiate.Fade(scenePolice, loadToColor, 0.7f);
    }
}
