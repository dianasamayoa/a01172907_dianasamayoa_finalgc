﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lights_controller : MonoBehaviour {

    [SerializeField] Light redLight;
    [SerializeField] Light blueLight;

    [SerializeField] Vector3 redLightTemp;
    [SerializeField] Vector3 blueLightTemp;

    [SerializeField] int speed;

    [SerializeField] float maxWait;
    [SerializeField] float minWait;

    private void Start()
    {
       // redLight = GetComponent<Light>();
        //blueLight = GetComponent<Light>();
        StartCoroutine(RotateLights());
        StartCoroutine(Flashing());
    }

    IEnumerator RotateLights()
    {
        yield return new WaitForSeconds (0.01f);

        redLightTemp.y += speed * Time.deltaTime;
        blueLightTemp.y -= speed * Time.deltaTime;

        redLight.transform.eulerAngles = redLightTemp;
        blueLight.transform.eulerAngles = blueLightTemp;
        StartCoroutine(RotateLights());
    }
    IEnumerator Flashing()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minWait,maxWait));
            redLight.enabled = !redLight.enabled;
            blueLight.enabled = !blueLight.enabled;
        }
    }

}
