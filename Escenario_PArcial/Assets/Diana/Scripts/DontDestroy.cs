﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour {

    private void Awake()
    {
        // the count prevent us of having two sounds playing at the same time
        GameObject[] objs = GameObject.FindGameObjectsWithTag("music");
        if (objs.Length > 2)
        {
            Destroy(this.gameObject);
            
        }
        DontDestroyOnLoad(this.gameObject);
    }
}
